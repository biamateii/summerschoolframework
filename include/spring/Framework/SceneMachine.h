#pragma once

#include <string>
#include <map>
#include <any>
#include <memory>

#include "qobject.h"
#include "qmainwindow.h"
#include "qcoreapplication.h"
#include "boost/any.hpp"

#include "spring/Framework/IScene.h"

namespace Spring
{
	class CSceneMachine : public QObject
	{
		Q_OBJECT
	public:
		CSceneMachine(std::map<const std::string, IScene*> a_SceneColllection,
			std::map<const std::string, const boost::any> a_TransientDataCollection,
			std::unique_ptr<QMainWindow> a_MainWindow,
			const std::string a_InitialScene);

		~CSceneMachine() = default;

	private:
		void getActiveScene(const std::string& a_Scene);

	public slots:
		void setActiveScene(const std::string& a_Scene);
		void release() const;

	protected:
		std::map<const std::string, IScene*> m_SceneColllection;
		std::map<const std::string, const boost::any> m_TransientDataCollection;
		const std::string m_sInitialScene;
		std::string mv_sCurrentScene;
		std::unique_ptr<QMainWindow> mv_uxMainWindow;
	};
}