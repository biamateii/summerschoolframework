#pragma once
#include "spring/Framework/IScene.h"
#include <map>
#include <string>

namespace Spring
{
	class IApplicationModel
	{

	public:

		virtual void defineScenes() = 0;
		virtual void defineTransientData() = 0;
		virtual void defineInitialScene() = 0;

		std::string getInitialSceneName() { return m_szInitialScene; }
		std::map<const std::string, const boost::any> getTransientData() { return m_TransientData; }
		std::map<const std::string, IScene*> getScene() { return m_Scene; }

		~IApplicationModel() { }

	protected:
		std::map<const std::string, IScene*> m_Scene;
		std::map<const std::string, const boost::any> m_TransientData;
		std::string m_szInitialScene;

	};
}