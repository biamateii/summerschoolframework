#pragma once
#include <map>
#include <memory>
#include <string>
#include <QtWidgets\QMainWindow>
#include <boost/any.hpp>

namespace Spring
{
	class IScene : public QObject
	{
		Q_OBJECT

	signals:
		void SceneChange(const std::string& ac_szSceneName);
	public:
		IScene(const std::string& ac_szSceneName)
			:m_sSceneName(ac_szSceneName)
		{}

		virtual void createScene() = 0;
		virtual void release() = 0;

		void drawScene()
		{
			QMetaObject::connectSlotsByName(m_uMainWindow.get());
		}
		void show()
		{
			m_uMainWindow->show();
		}
		void setWindow(std::unique_ptr<QMainWindow> av_xMainWindow)
		{
			m_uMainWindow = std::move(av_xMainWindow);
		}
		void setTransientData(std::map<const std::string, const boost::any> av_xTransientData)
		{
			m_TransientDataCollection = std::move(av_xTransientData);
		}
		std::unique_ptr<QMainWindow>& uGetWindow()
		{
			return m_uMainWindow;
		}
		std::map<const std::string, const boost::any> uGetTransientData()
		{
			return m_TransientDataCollection;
		}
		const std::string sGetSceneName()
		{
			return m_sSceneName;
		}

		virtual ~IScene() {};

	private:
		std::map<const std::string, const boost::any> m_TransientDataCollection;
		std::unique_ptr<QMainWindow> m_uMainWindow;
		const std::string m_sSceneName;

	};
};

