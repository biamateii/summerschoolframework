#pragma once
#include <memory>
#include <string>

#include <qmainwindow.h>

#include "spring/Framework/IApplicationModel.h"
#include "spring/Framework/SceneMachine.h"

namespace Spring
{
	class Application
	{
	public:
		~Application();
		static Application& getApplication();
		void setApplicationModel(IApplicationModel* av_xAppModel);
		void start(const std::string& av_szApplicationTitle, const unsigned ac_nWidth, const unsigned ac_nHeight);

	private:
		Application() = default;

	private:
		CSceneMachine* m_SceneMachine;
		IApplicationModel* mv_pAppModel;
		std::unique_ptr<QMainWindow> mv_xMainWindow;
	};
}
