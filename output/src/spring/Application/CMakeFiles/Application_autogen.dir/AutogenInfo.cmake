# Meta
set(AM_MULTI_CONFIG "TRUE")
set(AM_PARALLEL "4")
set(AM_VERBOSITY "")
# Directories
set(AM_CMAKE_SOURCE_DIR "F:/Others/spring-school-framework-structure")
set(AM_CMAKE_BINARY_DIR "F:/Others/spring-school-framework-structure/output")
set(AM_CMAKE_CURRENT_SOURCE_DIR "F:/Others/spring-school-framework-structure/src/spring/Application")
set(AM_CMAKE_CURRENT_BINARY_DIR "F:/Others/spring-school-framework-structure/output/src/spring/Application")
set(AM_CMAKE_INCLUDE_DIRECTORIES_PROJECT_BEFORE "")
set(AM_BUILD_DIR "F:/Others/spring-school-framework-structure/output/src/spring/Application/Application_autogen")
set(AM_INCLUDE_DIR "F:/Others/spring-school-framework-structure/output/src/spring/Application/Application_autogen/include_\$<CONFIG>")
set(AM_INCLUDE_DIR_Debug "F:/Others/spring-school-framework-structure/output/src/spring/Application/Application_autogen/include_Debug")
set(AM_INCLUDE_DIR_MinSizeRel "F:/Others/spring-school-framework-structure/output/src/spring/Application/Application_autogen/include_MinSizeRel")
set(AM_INCLUDE_DIR_RelWithDebInfo "F:/Others/spring-school-framework-structure/output/src/spring/Application/Application_autogen/include_RelWithDebInfo")
set(AM_INCLUDE_DIR_Release "F:/Others/spring-school-framework-structure/output/src/spring/Application/Application_autogen/include_Release")
# Qt
set(AM_QT_VERSION_MAJOR 5)
set(AM_QT_MOC_EXECUTABLE "F:/Qt/5.13.0/msvc2017_64/bin/moc.exe")
set(AM_QT_UIC_EXECUTABLE "")
# Files
set(AM_CMAKE_EXECUTABLE "C:/Program Files/CMake/bin/cmake.exe")
set(AM_SETTINGS_FILE "F:/Others/spring-school-framework-structure/output/src/spring/Application/CMakeFiles/Application_autogen.dir/AutogenOldSettings.txt")
set(AM_SETTINGS_FILE_Debug "F:/Others/spring-school-framework-structure/output/src/spring/Application/CMakeFiles/Application_autogen.dir/AutogenOldSettings_Debug.txt")
set(AM_SETTINGS_FILE_MinSizeRel "F:/Others/spring-school-framework-structure/output/src/spring/Application/CMakeFiles/Application_autogen.dir/AutogenOldSettings_MinSizeRel.txt")
set(AM_SETTINGS_FILE_RelWithDebInfo "F:/Others/spring-school-framework-structure/output/src/spring/Application/CMakeFiles/Application_autogen.dir/AutogenOldSettings_RelWithDebInfo.txt")
set(AM_SETTINGS_FILE_Release "F:/Others/spring-school-framework-structure/output/src/spring/Application/CMakeFiles/Application_autogen.dir/AutogenOldSettings_Release.txt")
set(AM_PARSE_CACHE_FILE "F:/Others/spring-school-framework-structure/output/src/spring/Application/CMakeFiles/Application_autogen.dir/ParseCache.txt")
set(AM_HEADERS "F:/Others/spring-school-framework-structure/include/spring/Application/ApplicationModel.h;F:/Others/spring-school-framework-structure/include/spring/Application/BaseScene.h;F:/Others/spring-school-framework-structure/include/spring/Application/InitialScene.h;F:/Others/spring-school-framework-structure/include/spring/Application/global.h")
set(AM_HEADERS_FLAGS "Mu;Mu;Mu;Mu")
set(AM_HEADERS_BUILD_PATHS "7G5R565DQL/moc_ApplicationModel.cpp;7G5R565DQL/moc_BaseScene.cpp;7G5R565DQL/moc_InitialScene.cpp;7G5R565DQL/moc_global.cpp")
set(AM_SOURCES "F:/Others/spring-school-framework-structure/src/spring/Application/ApplicationModel.cpp;F:/Others/spring-school-framework-structure/src/spring/Application/BaseScene.cpp;F:/Others/spring-school-framework-structure/src/spring/Application/InitialScene.cpp")
set(AM_SOURCES_FLAGS "Mu;Mu;Mu")
# MOC settings
set(AM_MOC_SKIP "")
set(AM_MOC_DEFINITIONS "Application_EXPORTS;QT_CORE_LIB;QT_GUI_LIB;QT_NO_DEBUG;QT_WIDGETS_LIB;WIN32")
set(AM_MOC_DEFINITIONS_Debug "Application_EXPORTS;QT_CORE_LIB;QT_GUI_LIB;QT_WIDGETS_LIB;WIN32")
set(AM_MOC_INCLUDES "F:/Others/spring-school-framework-structure/include;F:/Others/spring-school-framework-structure/include/spring/common;F:/Others/spring-school-framework-structure/bin;F:/Qt/5.13.0/msvc2017_64/include;F:/Qt/5.13.0/msvc2017_64/include/QtWidgets;F:/Qt/5.13.0/msvc2017_64/include/QtGui;F:/Qt/5.13.0/msvc2017_64/include/QtANGLE;F:/Qt/5.13.0/msvc2017_64/include/QtCore;F:/Qt/5.13.0/msvc2017_64/./mkspecs/win32-msvc")
set(AM_MOC_OPTIONS "")
set(AM_MOC_RELAXED_MODE "")
set(AM_MOC_MACRO_NAMES "Q_OBJECT;Q_GADGET;Q_NAMESPACE")
set(AM_MOC_DEPEND_FILTERS "")
set(AM_MOC_PREDEFS_CMD "")
