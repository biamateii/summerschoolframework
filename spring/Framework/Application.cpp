#include "..\..\..\include\spring\Framework\Application.h"

namespace Spring
{
	Application::~Application()
	{
		mv_pAppModel = nullptr;
		delete m_SceneMachine;
	}
	Application& Application::getApplication()
	{
		static Application l_application;

		return l_application;
	}

	void Application::setApplicationModel(IApplicationModel* av_xAppModel)
	{
		mv_pAppModel = av_xAppModel;

		mv_pAppModel->defineScenes();
		mv_pAppModel->defineTransientData();
		mv_pAppModel->defineInitialScene();

	}

	void Application::start(const std::string& av_szApplicationTitle, const unsigned ac_nWidth, const unsigned ac_nHeight)
	{
		mv_xMainWindow = std::make_unique<QMainWindow>();
		mv_xMainWindow->setWindowTitle(QString::fromUtf8(av_szApplicationTitle.c_str()));
		mv_xMainWindow->resize(ac_nWidth, ac_nHeight);

		m_SceneMachine = new CSceneMachine(mv_pAppModel->getScene(), mv_pAppModel->getTransientData(), std::move(mv_xMainWindow), mv_pAppModel->getInitialSceneName());
	}

}
