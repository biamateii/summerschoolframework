#include "spring/Framework/SceneMachine.h"

namespace Spring
{
	CSceneMachine::CSceneMachine(std::map<const std::string, IScene*> a_SceneColllection,
		std::map<const std::string, const boost::any> a_TransientDataCollection,
		std::unique_ptr<QMainWindow> a_MainWindow,
		const std::string a_InitialScene) :
		m_SceneColllection(a_SceneColllection),
		m_TransientDataCollection(a_TransientDataCollection),
		m_sInitialScene(a_InitialScene),
		mv_uxMainWindow(std::move(a_MainWindow)),
		mv_sCurrentScene("")
	{
		assert(m_SceneColllection.size() != 0);

		for (const auto& scene : m_SceneColllection)
		{
			QObject::connect(scene.second, SIGNAL(SceneChange(const std::string&)), this, SLOT(setActiveScene(const std::string&)));
		}

		getActiveScene(m_sInitialScene);

		QCoreApplication* app = QCoreApplication::instance();
		QObject::connect(app, SIGNAL(aboutToQuit()), this, SLOT(release()));
	}

	void CSceneMachine::getActiveScene(const std::string& a_Scene)
	{

		if (a_Scene != m_sInitialScene)
		{
			IScene* currentScene = nullptr;
			if (mv_sCurrentScene != "")
			{
				const auto& it = m_SceneColllection.find(mv_sCurrentScene);
				currentScene = it->second;

				mv_uxMainWindow = std::move(currentScene->uGetWindow());
				m_TransientDataCollection = currentScene->uGetTransientData();
			}

			const auto& nextScene = m_SceneColllection.find(a_Scene)->second;
			if (nextScene != nullptr)
			{
				if (currentScene != nullptr)
				{
					currentScene->release();
				}

				nextScene->setWindow(std::move(mv_uxMainWindow));
				nextScene->setTransientData(m_TransientDataCollection);
				nextScene->createScene();
				nextScene->drawScene();
				nextScene->show();

				mv_sCurrentScene = a_Scene;
			}
		}
	}

	void CSceneMachine::release() const
	{
		const auto currentScene = m_SceneColllection.find(mv_sCurrentScene);
		if (currentScene != m_SceneColllection.end())
			currentScene->second->release();
	}

	void CSceneMachine::setActiveScene(const std::string& a_Scene)
	{
		getActiveScene(a_Scene);
	}
}
