#include "spring\Application\ApplicationModel.h"

namespace Spring
{
	void CApplicationModel::defineScenes()
	{
		m_InitialScene = std::make_shared<InitialScene>("initialScene");
		m_Scene.emplace(std::make_pair("initialScene", m_InitialScene.get()));
	}
	void CApplicationModel::defineInitialScene()
	{
		m_szInitialScene = "initialScene";
	}
	void CApplicationModel::defineTransientData()
	{
		//TO DO
	}
}
