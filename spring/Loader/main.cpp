#include "qapplication.h"
#include "spring/Framework/Application.h"
#include "spring/Application/ApplicationModel.h"

int main(int argc, char** argv)
{
	QApplication app(argc, argv);
	auto& application = Spring::Application::getApplication();

	Spring::CApplicationModel a;
	application.setApplicationModel(&a);

	application.start("Da", 50, 50);

	app.exec();
}